<table align="center"><tr><td align="center" width="9999">
<img src="coretide-logo.png" align="center" width="150" alt="Project icon">

# The first DetectionOps open platform

_Threat Informed Detection Engineering_

</td></tr></table>

**🎤 Talks**

- [Lightning Talk] Hack.lu 2023 : [TIDeMEC(CoreTIDE) : A Detection Engineering Platform Homegrown At The EC](https://www.youtube.com/watch?v=lng-87nRTGQ)
- [Slides] FIRST Technical Colloquium Amsterdam 2024 : [CoreTIDE: the First Project of the OpenTIDE Family](https://www.first.org/resources/papers/amsterdam24/Benson-Housmann-Seguy-CoreTIDE-FIRST-TC-Amsterdam-2024.pdf) 

## A new era for Detection Engineering
CoreTide is a platform that has been built on thousands of manhours at the European Commission, and firetested for more than two years in a production environment before made Open Source. CoreTide is the backend of OpenTide, the overarching Detection Engineering framework made to empower Detection Engineering Teams.

### Features
- Highly mature and standardized, cross-system Detection-as-Code
- Powerful CI/CD architecture, where the client OpenTide instance injects CoreTide - decoupling code and content
- YAML-based Meta Schemas, defining all Objects within the OpenTide framework
- Powerul self-documenting JSON Schemas, creating a first of its kind IDE experience in the DE world
- Schemas, Templates, Indexes that all regenerate, and source from the client OpenTide instance configurations
- Object validation, Query Validation
- Documentation Self-Generation, creates a collection of interconnected markdown files 
- Full UUIDv4 Object system

